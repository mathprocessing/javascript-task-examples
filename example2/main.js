var urls = [
	'https://api.github.com/users/o0',
  'https://api.github.com/users/zeckson',
  'https://api.github.com/users/fyvfyv'
];

var template = document.querySelector('#btnTemplate').content;
var xhr = new XMLHttpRequest();

function downloadData(url) {
	xhr.onload = drawButton;
	xhr.send('GET', url);
}

function onClick(evt, data) {
	evt.preventDefault();
  document.body.insertAdjacentHTML('beforeend', '<p>' + 'ID: '+ evt.target.id + '\nName: ' + data.target.name + '</p>');
}

function drawButton(xhr, cb) {
	var data = JSON.stringify(xhr.response);
	var btnFragment = template.cloneNode(true);
  
  var btn = btnFragment.querySelector('button');
  btn.addEventListener('click', onClick(evt, data.name));
  btn.textContent = data.login;
  btn.setAttribute('id', data.id);
  
  document.body.appendChild(btn);
}

urls.forEach(function(url) {
	downloadData(url);
});